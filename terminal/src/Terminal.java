import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import java.util.*;

public class Terminal extends Applet implements WindowListener
{
	int cellSize = 50;
	int fieldSize = 10;
	int xMargin = 10;
	int yMargin = 15;
	Channel channel;
	Frame frame;
	Graphics graphics;
	ArrayList<String> cellDataList;
	int cellData [][];

	int startPosX = 1;
	int startPosY = 1;
	int curPosX = startPosX;
	int curPosY = startPosY;

	public static void main (String [] args)
	{
		Frame frame = new Frame ("AimCore Terminal");
		Terminal terminal = new Terminal ();
		frame.add ("Center", terminal);
		frame.addWindowListener (terminal);
		terminal.init (frame);
	}

	public void init (Frame frame)
	{
		this.frame = frame;
		frame.setSize (xMargin * 2 + cellSize * fieldSize + 11, yMargin * 2 + cellSize * fieldSize + 25);
		this.loadField ();
		frame.setVisible (true);
	}

	public void loadField ()
	{
		this.cellDataList = new ArrayList<String> ();
		this.cellData = new int [fieldSize + 1][fieldSize + 1];

		this.cellDataList.add (0, "        X ");
		this.cellDataList.add (1, "          ");
		this.cellDataList.add (2, "      X X ");
		this.cellDataList.add (3, "          ");
		this.cellDataList.add (4, "      X   ");
		this.cellDataList.add (5, "   X X    ");
		this.cellDataList.add (6, "  X       ");
		this.cellDataList.add (7, "          ");
		this.cellDataList.add (8, "X X       ");
		this.cellDataList.add (9, "          ");

/*
		this.cellDataList.add (0, "          ");
		this.cellDataList.add (1, "         X");
		this.cellDataList.add (2, "        X ");
		this.cellDataList.add (3, "       X  ");
		this.cellDataList.add (4, "      X   ");
		this.cellDataList.add (5, "     X    ");
		this.cellDataList.add (6, "    X     ");
		this.cellDataList.add (7, "   X      ");
		this.cellDataList.add (8, "  X       ");
		this.cellDataList.add (9, " X        ");
*/
		for (int x = 1; x <= fieldSize; x++)
		{
			for (int y = 1; y <= fieldSize; y++)
			{
				this.cellData [x][y] = 0;
			}
		}

		for (int i = 0; i < cellDataList.size (); i++)
		{
			int y = fieldSize - i;
			String s = cellDataList.get (i);
			byte chars [] = s.getBytes ();

			for (int j = 0; j < fieldSize; j++)
			{
				if (chars [j] == 88)
				{
					int x = j + 1;
					this.cellData [x][y] = 1;
				}
			}	
		}
	}

	public void paint (Graphics graphics)
	{
		this.graphics = graphics;

		for (int i = 0; i <= fieldSize; i++)
		{
			graphics.drawLine (xMargin + i * cellSize, yMargin, xMargin + i * cellSize, yMargin + cellSize * fieldSize);
		}

		for (int i = 0; i <= fieldSize; i++)
		{
			graphics.drawLine (xMargin, yMargin + i * cellSize, xMargin + cellSize * fieldSize, yMargin + i * cellSize);
		}

		for (int x1 = 1; x1 <= fieldSize; x1++)
		{
			for (int y1 = 1; y1 <= fieldSize; y1++)
			{
				if (this.cellData [x1][y1] == 1)
				{
					int x = xMargin + (x1 - 1) * (cellSize - 1) + x1;
					int y = yMargin + (fieldSize - y1) * (cellSize - 1) + (fieldSize - y1 + 1);
					graphics.setColor (new Color (150, 150, 150));
					graphics.fillRect (x, y, cellSize - 1, cellSize - 1);
				}
			}
		}

		int x = xMargin + cellSize * (fieldSize - (fieldSize - curPosX)) - cellSize / 2 - 10;
		int y = yMargin + cellSize * (fieldSize - curPosY + 1) - cellSize / 2 - 10;
		graphics.setColor (Color.black);
		graphics.fillRect (x, y, 20, 20);

		if (this.channel == null)
		{
			this.channel = new Channel (this);
			this.channel.connect ("127.0.0.1", 10101);
		}
	}

	public void callback (String data)
	{
		//System.out.println (data);
		String answer = "";
		String[] patterns = data.split ("\\^");

		for (int i = 0; i < patterns.length; i++)
		{
			switch (patterns[i])
			{
				case "MOVE_UP":
					if (this.cellData [this.curPosX][this.curPosY + 1] == 0)
					{
						answer += "^X" + this.curPosX + "Y" + this.curPosY + "=EMPTY";
						this.curPosY += 1;
						answer += "^X" + this.curPosX + "Y" + this.curPosY + "=ME";
					}
					break;
				case "MOVE_DOWN":
					if (this.cellData [this.curPosX][this.curPosY - 1] == 0)
					{
						answer += "^X" + this.curPosX + "Y" + this.curPosY + "=EMPTY";
						this.curPosY -= 1;
						answer += "^X" + this.curPosX + "Y" + this.curPosY + "=ME";
					}
					break;
				case "MOVE_RIGHT":
					if (this.cellData [this.curPosX + 1][this.curPosY] == 0)
					{
						answer += "^X" + this.curPosX + "Y" + this.curPosY + "=EMPTY";
						this.curPosX += 1;
						answer += "^X" + this.curPosX + "Y" + this.curPosY + "=ME";
					}
					break;
				case "MOVE_LEFT":
					if (this.cellData [this.curPosX - 1][this.curPosY] == 0)
					{
						answer += "^X" + this.curPosX + "Y" + this.curPosY + "=EMPTY";
						this.curPosX -= 1;
						answer += "^X" + this.curPosX + "Y" + this.curPosY + "=ME";
					}
					break;
			}
		}

		int x = xMargin + cellSize * (fieldSize - (fieldSize - curPosX)) - cellSize / 2 - 10;
		int y = yMargin + cellSize * (fieldSize - curPosY + 1) - cellSize / 2 - 10;
		graphics.setColor (Color.black);
		this.graphics.fillRect (x, y, 20, 20);
		super.paintComponents(this.graphics);
		super.repaint ();

		try
		{
			Thread.sleep (500);
		}
		catch (InterruptedException e)
		{
			System.out.println ("Ошибка управления потоком");
		}

		//System.out.println (answer);
		this.channel.send (answer);
	}

	public void windowActivated (WindowEvent windowEvent) {}

	public void windowDeactivated (WindowEvent windowEvent) {}

	public void windowIconified (WindowEvent windowEvent) {}

	public void windowDeiconified (WindowEvent windowEvent) {}

	public void windowOpened (WindowEvent windowEvent) {}

	public void windowClosing (WindowEvent windowEvent)
	{
		System.exit (0);
	}

	public void windowClosed (WindowEvent windowEvent) {}
}