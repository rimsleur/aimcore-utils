import java.net.*;
import java.io.*;

class Channel implements Runnable
{
	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;
	private Thread receiveThread;
	private Terminal terminal;

	Channel (Terminal terminal)
	{
		this.terminal = terminal;
	}

	public void connect (String host, int port)
	{
		try
		{
			this.socket = new Socket (host, port);
			this.in = new BufferedReader (new InputStreamReader (this.socket.getInputStream ()));
			this.out = new PrintWriter (this.socket.getOutputStream (), true);

			this.receiveThread = new Thread (this);
			this.receiveThread.start();
		}
		catch (IOException e)
		{
			System.out.println ("Ошибка соединения с сервером");
		}
	}

	public void run ()
	{
		while (1 == 1)
		{
			this.terminal.callback (this.receive ());
		}
	}

	public String receive ()
	{
		String data = "";
			
		try
		{
			while ((data = in.readLine ()) == null);
		}
		catch (IOException e)
		{
			System.out.println ("Ошибка получения сообщения");
		}

		return data;
	}

	public void send (String data)
	{
		out.println (data);
	}
}